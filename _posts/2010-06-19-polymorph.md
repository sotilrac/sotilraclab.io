---
layout: post
status: darft
published: true
title: We Have Polymorph!
author: Carlos
id: 638
wordpress_url: http://carlitoscontraptions.com/?p=638
date: '2010-06-19 21:09:35 -0400'
date_gmt: '2010-06-20 01:09:35 -0400'
categories:
- My Projects
tags:
- Polymorph
- Craft
---
{% include fig.html img="IMG_4617.jpg" caption="Polymorph" id=page.id %}

[Polymorph](http://en.wikipedia.org/wiki/Polycaprolactone "Polymorph") (or polycaprolactone) is a great nylon-like material that can be melted in hot water and manipulated with bare hands in order to model it into a desired shape. It is very useful when making quick (and not so quick) prototypes and custom parts for a project since it can be easily manipulated and molded and becomes very strong once it cools down.

{% include fig.html img="carlitoscontraptions.com" caption="Ring opening polymerization of Îµ-caprolactone to polycaprolactone" id=page.id %}

Unfortunately, this great material was not available for purchase in Canada (or at least not to my knowledge) and had to be ordered from the US wich was rather expensive in shipping costs.

All this has lately changed since [RobotShop](http://www.robotshop.com/ "RobotShop"), the Canadian based robot part distributor, included [Polymorph](http://www.robotshop.com/ProductInfo.aspx?pc=RB-Dag-09 "Polymorph") into their catalogue. Now Canadians can buy 1 kg of [polymorph](http://www.robotshop.ca/ProductInfo.aspx?pc=RB-Dag-09 "Polymorph") for roughly 44 CAD and have it quickly and cheaply delivered to their doorstep.

I bought some polymorph for my brother's birthday and we experimented with it as shown in the pictures below. Now, that Canada has polymorph, let all the Canadian go crazy and build awesome white (or [coloured](http://www.youtube.com/watch?v=u9TkpoTzFMo "Coloured Polymorph")) stuff.

{% include fig.html img="IMG_4620.jpg" caption="Polymorph + hot water" id=page.id %}

{% include fig.html img="IMG_4621.jpg" caption="Hot polymorph" id=page.id %}