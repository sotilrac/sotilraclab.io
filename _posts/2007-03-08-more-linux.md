---
layout: post
status: darft
title: More Linux
author: Carlos
id: 24
date: '2007-03-08 22:52:00 -0500'
date_gmt: '2007-03-09 05:52:00 -0500'
categories:
- Software
tags: []
---
The good thing about Linux is that it is light and manages the hardware well. This means that my computer runs fast nicely. This was not the case when I was running a commercial mainstream OS, my computer was so slow ans sluggish that i thought I needed a newer faster one.

Now I can run heavy applications such as Azureus without any trouble (before, I had to use Bitcomet since Azureus was really really excruciatingly slow). I can even run lots of applications at the same time fast and reliably.

{% include fig.html img="scale.png" caption="" id=page.id %}

Also, the startup time is pretty fast.

## Desktop Environments

I thought Linux did not have any eye candy and that eye candy is heavy and makes your machine slow. Wrong. You can configure pretty much everything of the [Gnome](http://www.gnome.org/) appearance. There are skins, wallpapers that support transparencies (PNG SVG), icons of any size you wish (PNG, SVG), nice transparent docks, etc.

{% include fig.html img="resize+icon.png" caption="" id=page.id %}  

{% include fig.html img="panels.png" caption="" id=page.id %}

For those who have a 3D graphics card (like me), there is [Beryl](http://www.beryl-project.org/). Beryls is truly beautiful. There are lots of screencasts on the net about it so I'll let the video speak for itself.  
  
Amazingly enough all this beautiful eye candy works on my computer pretty well.  

For further and really extensive customization there is [KDE](http://www.kde.org/). If Gnome is configurable, then KDE is the definition of configuration itself. Pretty much all the parameters can be easily changed through a GUI in order to obtain a desktop experience that fully suits your personal needs and taste.

{% include fig.html img="KDE1.png" caption="" id=page.id %}

Here are some extra screenshots of Nautilus (the gnome file manager) and Konqueror (the KDE file manager, FTP/SSH client, internet browser, image viewer and coffee machine). Notice the nice nautilus thumbnails, the function packed Konqueror and the tight integration between the KDE applications (e.g. Konqueror and Akregator).

{% include fig.html img="nautilus.png" caption="" id=page.id %}

{% include fig.html img="thumbnails.png" caption="" id=page.id %}

{% include fig.html img="Konqueror.png" caption="" id=page.id %}

{% include fig.html img="Konqueror1.png" caption="" id=page.id %}
