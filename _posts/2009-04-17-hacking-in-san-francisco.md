---
layout: post
status: darft
published: true
title: Hacking in San Francisco
author: Carlos
id: 76
wordpress_url: http://carlitoscontraptions.com/?p=130
date: '2009-04-17 17:30:02 -0400'
date_gmt: '2009-04-17 21:30:02 -0400'
categories:
- My Projects
tags:
- Travel
---
{% include fig.html img="img_2007.jpg" caption="Hacking in SF" id=page.id %}

I had the chance to spend a week in San Francisco a month ago (which, by the way, is a beautiful city).

For some reason, everywhere I go, I end up having to repair, re-purpose, or build something. In this case, my [Eee PC](http://carlitoscontraptions.com/2008/09/eee-pc-1000-ubuntu-kde-41/ "My Eee PC")'s charger broke and I had to fix it.

## Flashback

I lost my original charger some time ago and I had to buy a new one. Since it was an urgent need, I resorted to buy a cheap one that I could get fast and order the official one (which was much more expensive) just to make sure I would not have the problem of being without a charger again. Both the cheap and the official one performed similarly and I did not feel the need to use the official one too much (so it could be better preserved).

## Back to the present

When I packed up for the trip, I took the cheap charger and left the official one at home. That was a big, big, stupid mistake.

When I got to the Montreal airport and was waiting to board the plane, I decided to [write something](http://carlitoscontraptions.com/2009/03/compact-keychain-20/ "Compact Keychhain 2") for this blog and I came to the sad realization that my charger was not working. Fortunately, the Eee PC has a very long battery life and I manage preserve the energy for a few days.

{% include fig.html img="img_2009.jpg" caption="Modified Charger" id=page.id %}

In San Francisco, I got a 12 Vdc , 3A (the required rating) charger at a stolen goods surplus store. With my trusty Swiss Army knife (the only tool I had) I changed the power connector and taped it with some electrical tape I got at a Dollar store.

Parallel to that, my good friend Guillaume, who was hosting me at his house, had a broken toy horse. So we decided to repair it so it could be functional and rideable again. The horse also had a broken electronic part that was supposed to emulate the galloping noise.

Using some left over wire from my broken charger and some tape we were able to make the electronic part work again. The mechanical part was also easily repaired by replacing the rotten wooden rods with some new ones.

{% include fig.html img="img_2008.jpg" caption="Horse Circuit" id=page.id %}

In the end, I was glad to be building and tinkering in San Francisco.

I also got the amazing chance of visiting [Noise Bridge](https://noisebridge.net "Noise Bridge"), a very nice hackerspace with lots of cool toys and great people.

If you are interested, [my gallery](http://photo.carlitoscontraptions.com "Carlitos' Gallery") is filled with more than 1000 pictures from San Francisco.

{% include fig.html img="img_2013.jpg" caption="Toy horse back in action." id=page.id %}