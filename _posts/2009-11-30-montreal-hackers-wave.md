---
layout: post
status: darft
published: true
title: Montreal Hackers Wave
author: Carlos
id: 90
date: '2009-11-30 13:17:04 -0500'
date_gmt: '2009-11-30 17:17:04 -0500'
categories:
- General
- Info
tags:
- Software
- Montreal
---
{% include fig.html img="wave.goo" caption="Google Wave" id=page.id %}

I finally got a [Google Wave](http://www.wave.google.com/ "Google Wave") account and I decided to do something for the community. More precisely, the hacker/tinkerer/DIY community.

I think it would be rather interesting to have a place where to share, showcase and discuss projects and hacks with local people (from Montreal).

If you want to join the wave you can find it [here](https://wave.google.com/wave/#restored:wave:googlewave.com!w%252BPUXPJxujA "Montreal's Hackers Wave").

I hope this will result in a stronger more connected Montreal Hacker community.

Cheers, and see you on the wave!

I have a few Wave invites left so if any geeky Montrealer would like one, please leave a comment and I'll see what I can do.