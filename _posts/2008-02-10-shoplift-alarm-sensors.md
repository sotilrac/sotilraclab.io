---
layout: post
status: darft
title: Shoplift Alarm Sensors
author: Carlos
id: 43
date: '2008-02-10 16:45:00 -0500'
date_gmt: '2008-02-10 23:45:00 -0500'
categories:
- Info
tags: []
---
{% include fig.html img="ss850544.jpg" caption="" id=page.id %}

I have seen many times those little anti-theft devices stuck to PDAs, digital cameras and all sorts of small consumer electronics products and I always wondered how they worked and imagined they were very complicated systems involving delicate glass switches (for detecting violent tinkering), optical proximity sensors (for detecting the change of distance/reflectivity between the device and the protected product), impedance meters on the alarm system base (for detecting the change of impedance when someone replaces the device by an equivalent circuit), etc.

The other day I was lucky enough to see (and even photograph) a broken shoplift "sensor", as they are called. I discovered that they are very simple and not that secure.

## Here are my findings

### Original Pictures:

{% include fig.html img="ss850541.jpg" caption="" id=page.id %}

{% include fig.html img="ss850540.jpg" caption="" id=page.id %}  

### How I think the system works:  

{% include fig.html img="shoplift+tag.png" caption="" id=page.id %}

The above diagram is self explanatory and it shows how simple and easy to defeat they are. The system only involves a simple metal switch and a big plastic button to operate it.