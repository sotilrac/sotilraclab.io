---
layout: post
status: darft
published: true
title: Carlitos' Youtube Interview
author: Carlos
id: 926
wordpress_url: http://carlitoscontraptions.com/?p=926
date: '2011-05-13 00:19:39 -0400'
date_gmt: '2011-05-13 04:19:39 -0400'
categories:
- My Projects
tags:
- Nao
---
[Benoit](http://www.benoitchamontin.com/) from [Geeks and Com](http://www.geeksandcom.com/) was kind enough to interview me a while back at the [Geekfest](http://carlitoscontraptions.com/tag/geekfest/). I am not sure I perform particularly well in an interview but I guess this might be interesting for those who want to know more about [Nao](http://carlitoscontraptions.com/tag/nao/). BTW, the interview is in French.  

{% include youtube.html id="Sr3iPzYZSOE" %}

Oh! I forgot. The thumbnail is hilarious.