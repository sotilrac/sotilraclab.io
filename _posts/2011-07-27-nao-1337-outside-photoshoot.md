---
layout: post
status: darft
published: true
title: First Time Nao 1337 Goes Outside, Photoshoot
author: Carlos
id: 1056
wordpress_url: http://carlitoscontraptions.com/?p=1056
date: '2011-07-27 23:04:40 -0400'
date_gmt: '2011-07-28 03:04:40 -0400'
categories:
- My Projects
- Robotics
tags:
- Montreal
- Photography
- Nao
---
{% include fig.html img="IMG_0788.jpg" caption="Nao 1337 and Carlitos Downtown" id=page.id %}

We went downtown Montreal the other day with Nao 1337 and shot some pictures, 
this was mainly an exercise in vanity. See the resulting photo gallery below.

[gallery]
