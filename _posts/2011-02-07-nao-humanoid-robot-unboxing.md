---
layout: post
status: darft
published: true
title: Nao Humanoid Robot Unboxing
author: Carlos
id: 830
wordpress_url: http://carlitoscontraptions.com/?p=830
date: '2011-02-07 22:43:53 -0500'
date_gmt: '2011-02-08 02:43:53 -0500'
categories:
- My Projects
- Robotics
tags:
- Nao
- Developer Program
- Aldebaran
- Unboxing
---
I got my Nao!

See the video below for a sneak peek of the robot. This video shows you what you would get if you were to join the Aldebaran Developer program.

{% include youtube.html id="_R1PbsjTkwg" %}

Stay posted for more Nao action. You can also visit my {% include youtube.html id="channel" %}(http://www.youtube.com/user/fcaneo2) in order to see my latest Nao videos.
