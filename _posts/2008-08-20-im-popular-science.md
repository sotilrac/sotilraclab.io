---
layout: post
status: darft
published: true
title: I'm Popular (Science)
author: Carlos
id: 53
wordpress_url: http://carlitoscontraptions.com/?p=58
date: '2008-08-20 17:32:00 -0400'
date_gmt: '2008-08-21 00:32:00 -0400'
categories:
- General
tags: []
---
{% include fig.html img="pop_sci.png" caption="" id=page.id %}

My [compact keychain](http://carlitoscontraptions.com/2007/03/compact-keychain/ "Compact Keychain") is featured in the August edition of the Popular Science magazine. Needless to say, I'm very happy about this.

Granted, it is a small article but it is still impressive to see how far blog posts can go.