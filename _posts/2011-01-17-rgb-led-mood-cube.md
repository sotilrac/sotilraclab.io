---
layout: post
status: darft
published: true
title: Arduino-controlled RGB LED Mood Cube
author: Carlos
id: 795
wordpress_url: http://carlitoscontraptions.com/?p=795
date: '2011-01-17 01:16:40 -0500'
date_gmt: '2011-01-17 05:16:40 -0500'
categories:
- My Projects
- Arduino
tags:
- Carlitos' Project
- Arduino
- Rainbowduino
- RGB LED
---
As the first [Carlitos' Project](http://carlitoscontraptions.com/tag/carlitos-project/), I put together a 4x4x4 RGB LED Mood Cube. This cube is composed of 64 RGB LEDs that can generate any colour you can imagine.

{% include fig.html img="carlitoscontraptions.com" caption="RGB LED Mood Cube" id=page.id %}

The main idea is to display colourful patterns and nice animations in this 3D LED matrix. In order to do so, the LEDs are controlled by a [Rainbowduino](http://www.robotshop.com/ProductInfo.aspx?pc=RB-See-76), the lovechild of an [Arduino](http://carlitoscontraptions.com/project/arduino/) and an LED driver. The Rainbowduino can power up to 192 LEDs which is exactly the number required for this project (4x4x4x3 = 192).

See the instructional video below if you want to put together a cube of your own. Be prepared to watch me do a lot of soldering at 10x speed.

\[youtube _-zgh6amwbM#\]

In order to access the full instructions and documentation for this project please visit the [RGB LED Mood Cube project page](http://www.robotshop.com/gorobotics/articles/microcontrollers/carlitos-project-rgb-led-mood-cube) it put together over at GoRobotics.net.

{% include fig.html img="carlitoscontraptions.com" caption="RGB LED Mood Cube Kit" id=page.id %}

If you want to make your own cube and are prepared to solder a lot, you might want to have a look at the [full RGB LED Mood Cube Kit](http://www.robotshop.com/ProductInfo.aspx?pc=RB-Rbo-87) we put together at RobotShop.