---
layout: post
status: darft
title: Quick and Cheap Laptop Lamp
author: Carlos
id: 17
date: '2006-12-05 23:30:00 -0500'
date_gmt: '2006-12-06 06:30:00 -0500'
categories:
- My Projects
tags: []
---
I built this quick laptop lamp some time ago for my mom. It is very simple and requires very few materials.

## Materials

*   1 type A male [USB](http://en.wikipedia.org/wiki/USB) plug
*   1 light bulb (that works well at around 5V)
*   1 lamp body (I found mine in the garbage)

First, I thoroughly cleaned, sanded, and polished the lamp body (which comes from a sort of old reading lamp).

Once the body was clean and shiny (it was completely black before) , I removed the lamp power connector and replaced it with the USB plug. In order to achieve this, the red (pin 1) terminal must be connected to the positive lead and the black (pin 4) terminal, to the lamp ground.

The lamp is basically done.

{% include fig.html img="laptop lamp.jpg" caption="" id=page.id %}

Since I did not have the appropriate light bulb for the lamp I had to adapt a bulb by adding a solder blob on its side. Now it fits perfectly.

{% include fig.html img="light.jpg" caption="" id=page.id %}

{% include fig.html img="laptop lamp closeup.jpg" caption="" id=page.id %}

This lamp won't be as power efficient as the [LED ones](http://img.alibaba.com/photo/50490807/Laptop_USB_LED_Reading_Light.jpg) but it is cheap, quick and very simple to build. Enjoy.

{% include fig.html img="laptop lamp in action.jpg" caption="" id=page.id %}