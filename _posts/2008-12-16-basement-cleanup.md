---
layout: post
status: darft
published: true
title: Basement Cleanup
author: Carlos
id: 64
wordpress_url: http://carlitoscontraptions.com/?p=64
date: '2008-12-16 21:09:00 -0500'
date_gmt: '2008-12-17 04:09:00 -0500'
categories:
- General
- Tools
tags: []
---
I finally cleaned up my basement/workshop and put some order into my tools and materials. My main source for materials in general is the garbage as the faithful readers may already know. Too bad I was too late for the [Hacked Gadgets Workbench Contest](http://hackedgadgets.com/2008/11/16/hacked-gadgets-workbench-contest-winner/).

{% include fig.html img="img_0964.jpg" caption="" id=page.id %}

## My new soldering space

{% include fig.html img="img_1034.jpg" caption="" id=page.id %}

I also got some new tools a while ago. Note the precision screw driver holder made out of a plastic jar cap and the screw driver stand made out of a piece of wood I found in the garbage.

{% include fig.html img="img_1032.jpg" caption="" id=page.id %}

I also got some new measuring tools that are extremely useful (and make me extremely happy).

{% include fig.html img="img_1051.jpg" caption="" id=page.id %}

## Some of my electronics parts:

{% include fig.html img="img_1037.jpg" caption="" id=page.id %}

{% include fig.html img="img_1053.jpg" caption="" id=page.id %}