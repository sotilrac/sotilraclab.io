---
layout: post
status: darft
title: Engineering Bling
author: Carlos
id: 44
date: '2008-03-17 01:53:00 -0400'
date_gmt: '2008-03-17 08:53:00 -0400'
categories:
- General
tags: []
---
Finally, I got the ultimate (engineering) bling: The Iron Ring.

{% include fig.html img="ss850982.jpg" caption="" id=page.id %}

Needless to say, I'm happy. Also, since this means that now that I'm an engineer (only morally, not legally), I should post some nice complex projects instead of the simple things I normally blog about.

{% include fig.html img="ss850978.jpg" caption="" id=page.id %}

Below you can enjoy a few pictures of my homies (David and Cynthia) and I.

{% include fig.html img="ss850953.jpg" caption="" id=page.id %}  

{% include fig.html img="ss850947.jpg" caption="" id=page.id %}

Note the (rather clever) use of gangster slang to denote how "fresh" we have become now that we have the ring ;)