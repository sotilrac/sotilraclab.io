---
layout: post
status: darft
published: true
title: I'm On New Scientist!
author: Carlos
id: 1173
wordpress_url: http://carlitoscontraptions.com/?p=1173
date: '2012-06-07 19:48:23 -0400'
date_gmt: '2012-06-07 23:48:23 -0400'
categories:
- Robotics
- News
tags:
- New Scientist
---
{% include fig.html img="www.new" caption="New Scientist - 21/01/2012" id=page.id %}

I was interviewed and featured in an article about the future of robotics on the [New Scientist magazine from January 2012](http://www.newscientist.com/issue/2848). The article was written by the awesome [Celeste Biever](http://www.newscientist.com/search?rbauthors=Celeste+Biever).

{% include fig.html img="IMG_20120608_001912.jpg" caption="New Scientist article" id=page.id %}

Needless to say, I am truly humbled to have been quoted among so many amazing roboticists including Andrew Ng (my dear [ML Class](https://www.coursera.org/course/ml) professor).
