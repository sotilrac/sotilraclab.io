---
layout: post
status: darft
title: Supercapacitors
author: Carlos
id: 25
date: '2007-03-08 21:47:00 -0500'
date_gmt: '2007-03-09 04:47:00 -0500'
categories:
- General
tags: []
---
Today I received some supercapacitors. I'll will use them in an upcoming *TOP SECRET* project.

{% include fig.html img="box.jpg" caption="" id=page.id %}

I have got a [10F,](http://www.cooperet.com/library/products/PS-5102 B Series.pdf) a 22F and a 50F capacitors.

Here are some pictures:

{% include fig.html img="capacitorsbox.jpg" caption="" id=page.id %}

{% include fig.html img="supercapacitors.jpg" caption="" id=page.id %}

By the way, please only order free samples if you need them.
