---
layout: post
status: darft
title: Bogotá Rake
author: Carlos
id: 16
date: '2006-12-04 21:44:00 -0500'
date_gmt: '2006-12-05 04:44:00 -0500'
categories:
- My Projects
tags: []
---
A Bogotá rake is a very effective [lock picking](http://en.wikipedia.org/wiki/Lock_picking) tool (named after the city of itś creation?). It is surprisingly easy to build and allows you to open lots of locks very quickly and easily. I have been able to open many locks just minutes after having this little tool done (including standard door and bicycle locks).

For this project you only need one [street sweeper](http://en.wikipedia.org/wiki/Street_cleaner) bristle. You can find these on the streets after a street sweeper passed by. Their rotating brushes leave behind these precious bristles.

{% include fig.html img="bristle.jpg" caption="" id=page.id %}  
First you need a template. I found this very good one from [Exodus5000](http://www.lockpicking101.com/profile.php?mode=viewprofile&u=2273).  

{% include fig.html img="bogota template.jpg" caption="" id=page.id %}

You have to print it at its original size (@ 500 pixels/inch) onto a piece of paper, cut it using scissors or some other cutting device, and paste the paper using normal glue onto the bristle (after it has been cleaned of course). You should get something like this:

{% include fig.html img="IMG_0018.jpg" caption="" id=page.id %}  

Then, using a permanent marker paint around it and remove the paper.

Now, use some files to remove all the painted sections until you get the desired shape.

{% include fig.html img="bogota closeup.jpg" caption="" id=page.id %}

Sand it very thoroughly. It is very important. First use a medium grit sandpaper and the a fine one. In order to sand all the curvy sections, I used a piece of sandpaper rolled around a medium nail. In the end, it should be smooth as ...hum... as something very very smooth. This allows for a fluid motion of the Bogotá inside the lock (important if you want to get the lock open).

I accidentally broke my bristle. With the broken piece I built a tension wrench. it is a bit short, but it works well. If you want one, just bend the bristle (not to much or you may snap it).

{% include fig.html img="tension wrench.jpg" caption="" id=page.id %}

For those who don't already know, a tension wrench is used to apply a small tension to the lock while you pick it. See [this article](http://en.wikipedia.org/wiki/Tension_wrench) for more information.

With these tools you can now start a life of crime. You should be able to open simple locks (and perhaps more complex ones) in a couple of minutes. Simply apply a small tension to the lock (as if turning it with a key) using the tension wrench, and jiggle the Bogotá inside the lock rapidly and randomly. The lock should open in less than a minute (keep trying if it doesn't).

{% include fig.html img="bogota2.jpg" caption="" id=page.id %}

Disclaimer: I do not condone leading a criminal life. However, if my blog does inspire you to become some sort of criminal and you succeed well at it, please share a bit of your earnings with me. After all, you will be doing well thanks to me.