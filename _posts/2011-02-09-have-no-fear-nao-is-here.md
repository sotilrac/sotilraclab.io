---
layout: post
status: darft
published: true
title: Have No Fear, Nao is Here
author: Carlos
id: 828
wordpress_url: http://carlitoscontraptions.com/?p=828
date: '2011-02-09 00:34:21 -0500'
date_gmt: '2011-02-09 04:34:21 -0500'
categories:
- My Projects
- Robotics
tags:
- Nao
- Developer Program
- Aldebaran
---
{% include fig.html img="carlitoscontraptions.com" caption="Nao 1337 Waves" id=page.id %}

I named him Nao [1337](http://en.wikipedia.org/wiki/Leet) but I am open to suggestions. You can leave a comment below if you think of a better name. You can see a quick presentation of this awesome humanoid robot below.

\[youtube -GwpDdNeuLM\]

I'll be posting further developments, images and pictures as I progress. I am planning on making several applications in order to achieve my ultimate goal of having Nao play chess.

{% include fig.html img="carlitoscontraptions.com" caption="Nao 1337 Stands Up" id=page.id %}

### The Bandana

As you can see in the pictures and videos, Nao1337 wears a red bandana. This is manly because he is bad-ass and utterly cool.

{% include fig.html img="carlitoscontraptions.com" caption="Nao 1337 Sitting" id=page.id %}