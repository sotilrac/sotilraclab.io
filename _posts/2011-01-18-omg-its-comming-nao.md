---
layout: post
status: darft
published: true
title: OMG! It's Coming Nao!
author: Carlos
id: 817
wordpress_url: http://carlitoscontraptions.com/?p=817
date: '2011-01-18 01:45:09 -0500'
date_gmt: '2011-01-18 05:45:09 -0500'
categories:
- My Projects
- Robotics
tags:
- Nao
- Developer Program
---
{% include fig.html img="carlitoscontraptions.com" caption="Nao's Packing Picture" id=page.id %}

I joined [Aldebaran's developer program](http://www.robotshop.com/blog/aldebaran-developer-program-the-future-is-nao-1137) and now my Nao is coming (much faster than I expected). They even sent me a picture of the box prior to shipping it.

This is an amazing experience and you can be sure I'll be sharing all of it in here.

Stay tuned for more Nao news!

{% include youtube.html id="RpmKsjhw2sA" %}
