---
layout: post
status: darft
published: true
title: Steampunk Gun and Goggles
author: Carlos
id: 1171
wordpress_url: http://carlitoscontraptions.com/?p=1171
date: '2012-06-09 00:47:45 -0400'
date_gmt: '2012-06-09 04:47:45 -0400'
categories:
- My Projects
tags:
- Cosplay
---
I was invited to a Pride & Prejudice & Zombies themed party and I thought that (although it is not exactly the same era) I could have a steampunk disguise and be a steampunk zombie hunter.

{% include fig.html img="IMG_20120527_032921_Carl_Gritty_Trashy.jpg" caption="Steampunk Zombie Hunter" id=page.id %}

I wanted to have a steampunk disguise for a long time and this was the perfect excuse to do it, or actually to have my brother, Alan, do it for me. While I worried about the clothes, he built an awesome steampunk gun based on a cheap Nerf knock-off and some goggles using plumbing parts. See the awesome results below.

\[gallery link="file" order="DESC"\]
