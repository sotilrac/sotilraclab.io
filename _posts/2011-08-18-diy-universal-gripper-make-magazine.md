---
layout: post
status: darft
published: true
title: DIY Universal Gripper in Make Magazine
author: Carlos
id: 1085
wordpress_url: http://carlitoscontraptions.com/?p=1085
date: '2011-08-18 22:09:13 -0400'
date_gmt: '2011-08-19 02:09:13 -0400'
categories:
- News
tags:
- Press
- Make
---
I am very happy to announce that my [DIY universal gripper project](http://carlitoscontraptions.com/2010/10/diy-universal-robot-gripper/) has been featured in Make magazine Volume 27. See the article below. Also check out [my updated version of the gripper.](http://carlitoscontraptions.com/2011/05/diy-universal-jamming-gripper-2-0/)

{% include fig.html img="carlitoscontraptions.com" caption="Make Volume 27 Page 78" id=page.id %}

I am very happy about this, the only issue is that for some reason, they think my name is Carlos Asmat Jr., but there is no such thing as "Jr." in my name.
