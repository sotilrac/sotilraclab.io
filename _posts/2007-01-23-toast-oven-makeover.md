---
layout: post
status: darft
title: Toast Oven Makeover
author: Carlos
id: 20
date: '2007-01-23 15:46:00 -0500'
date_gmt: '2007-01-23 22:46:00 -0500'
categories:
- My Projects
tags: []
---
My girlfriend had an old toast oven (about 20 years old). Slowly it decayed and eventually, broke (the timer did not work any more but it still heats).

What to do in such a situation when you find yourself to be the most amazingly kind boyfriend in the entire universe known to man?

The answer is simple, and it can be reduced to three easy steps:

1.  Find another toast oven in the trash (it must be the exact same model than your girlfriend's).
2.  Clean it thoroughly (and I mean THOROUGHLY. It should shine like a mirror, inside and out).
3.  Give it to your girlfriend.

Note: in step No. 2, you should make sure the oven is in excellent working condition. There would be no point in cleaning it if it was broken (besides the intrinsic pleasure of cleaning a 20 years old oven).

IMHO, the easiest way to clean it is to take it apart, clean each piece, and put it it back together (but I have not tried any other method).

Enjoy some before and after shots:

## Before...
{% include fig.html img="Before1.jpg" caption="" id=page.id %}

{% include fig.html img="Before2.jpg" caption="" id=page.id %}

## After...
{% include fig.html img="After1.jpg" caption="" id=page.id %}

{% include fig.html img="After2.jpg" caption="" id=page.id %}