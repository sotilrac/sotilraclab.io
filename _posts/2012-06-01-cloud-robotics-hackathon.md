---
layout: post
status: draft
title: Cloud Robotics Hackathon
author: Carlos
id: 1161
wordpress_url: http://carlitoscontraptions.com/?p=1161
date: '2012-06-01 19:44:22 -0400'
date_gmt: '2012-06-01 23:44:22 -0400'
categories:
- My Projects
- Robotics
- News
tags:
- Cloud Robotics
- Hackathon
---
Back in March we organized the first [Global Cloud Robotics Hackathon](http://roboticshackathon.com/). Along with co-organizer Sara Ahmadian, and all the local and global collaborators, we brought together around 200 people in eight locations around the world.

The main goal of the hackathon is to allow programmers to jump into the robotics world through the use of the new and exciting field of Cloud Robotics.

{% include fig.html img="cloud_robotics_hackathon.png" caption="Cloud Robotics Hackathon" id=page.id %}

You can see all the information about the event in [its official](http://roboticshackathon.com/) website as well as the resulting presentations in the [Cloud Robotics Hackathon YouTube Channel](http://www.youtube.com/user/RoboticsHackathon).

For sure, there will be more iterations of this hackathon in the future!
