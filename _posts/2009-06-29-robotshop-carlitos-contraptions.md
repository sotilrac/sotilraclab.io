---
layout: post
status: darft
published: true
title: RobotShop + Carlitos' Contraptions
author: Carlos
id: 78
wordpress_url: http://carlitoscontraptions.com/?p=215
date: '2009-06-29 13:24:09 -0400'
date_gmt: '2009-06-29 17:24:09 -0400'
categories:
- General
tags: []
---
{% include fig.html img="www.rob" caption="RobotShop.com" id=page.id %}

The robotics parts retailer [RobotShop](http://www.robotshop.com/ "RobotShop.com") will be sponsoring robotics projects at [Carlitos' Contraptions](http://carlitoscontraptions.com "CarlitosContraptions.com"). As a result I'll be rolling out robotics projects for the next weeks/months.

I'll try to make simple projects at the beginning and build up the difficulty level as I go. The idea is that, if you completed the the first projects, you will be able to reuse the skills/parts/code to build the future projects.

Of course, all articles are going to be published under the usual Creative Commons license and the code is going to be published under the GPL license.

I am planning to use [Python](http://www.python.org/ "Python.org") for any program running on a computer and [Arduino](http://www.arduino.cc/ "Arduino.cc") as a micro-controller platform. These choices aim at making the projects as open and as accessible as possible. Also, they ensure that they are perfectly cross-platform.

As always, I am open to any suggestions or wishes regarding the projects.

*Note*: the [RobotShop](http://www.robotshop.com/ "RobotShop.com") logo above is not published under the CC license.