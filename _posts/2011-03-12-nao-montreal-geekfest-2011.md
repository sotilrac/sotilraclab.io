---
layout: post
status: darft
published: true
title: Nao at the Montreal Geekfest 2011
author: Carlos
id: 873
wordpress_url: http://carlitoscontraptions.com/?p=873
date: '2011-03-12 10:53:57 -0500'
date_gmt: '2011-03-12 14:53:57 -0500'
categories:
- News
tags:
- Nao
- Geekfest
---
{% include fig.html img="IMG_0287.jpg" caption="Carlos, Alan and Nao 1337 at the Geekfest" id=page.id %}

[As announced before](http://carlitoscontraptions.com/2011/02/geekfest/), we attended the Montreal Geekfest and Nao 1337 performed some dances and demos for all fellow geeks. We were placed right at the entrance and Nao 1337 had the mission of giving away free Geekfest pins. He could not do this though, since people preferred to see him greet them and dance. I have to thank my brother Alan for coming along and helping me with the presentation.

{% include fig.html img="IMG_0266.jpg" caption="Nao 1337 chills out at the Geekfest" id=page.id %}

Nao 1337 was very popular with pretty much everybody and we made lots of friends. Nao got some Ubuntu stickers, could sit on a Star Wars cannon, and even learned a magic trick. In the picture above you see Nao 1337 actually chilling out (his motors were hot after some intense dancing).

{% include youtube.html id="4YEgARjz2ME" %}

As you can see towards the end of the video above, Nao 1337 can perform a very cool magic trick. I believe it is the first robot to do magic or at least to assist a magician. Leave a comment if you do not agree.

{% include fig.html img="IMG_0270.jpg" caption="Nao gets some firepower" id=page.id %}

{% include fig.html img="IMG_0268.jpg" caption="Nao wows family" id=page.id %}

The Geekfest was a lot of fun, you can see some more pictures of Carlitos' Contraptions at the Geekfest here:

*   [Magicol Media](http://www.magicolmedia.com/?p=11566)
*   [Geekfestmtl Flickr Pool](http://www.flickr.com/groups/geekfestmtl/pool/)
*   [Branchez-vous](http://www.branchez-vous.com/techno/actualite/2011/03/nao-robot-humanoide-aldebaran-robotics-robotique-geekfest-mtl-2011.html)

\[gallery link="file" order="DESC" columns="4"\]
