---
layout: post
status: public
title: Blog Makeover
author: Carlos
id: 143
date: '2019-01-08 01:01:01 -0500'
categories:
- News
tags:
- web
---
After a five year hiatus, the blog is back, and emptier than ever.

I ignored my blog for long enough and it got infested with Spam. The old WordPress posts database got injected with male enhancement links and generally mangled.

To avoid this from happening again, I decided to remake the blog, but this time 100% more static. Gone are the days of WordPress, the databases, and canned yet slightly customized themes. I'm now using Jekyll, the static site generator with my custom CSS based on Boostrap (as any stylish developer would).

{% include fig.html img="https://m.media-amazon.com/images/M/MV5BMDdlZTc2NmItNzE2MS00YTgzLTg5NjAtMDk4MzZmMTA0ZDlmL2ltYWdlXkEyXkFqcGdeQXVyNzc5NjM0NA@@._V1_.jpg" caption="Dr. Jekyll (1939)" id=page.id %}

In a very manual and laborious way, I stripped all the injected Spam from every post, reformatted them in Markdown and hosted them in a [git repo](https://gitlab.com/sotilrac/sotilrac.gitlab.io). It turns out I had many more posts that I remembered when I started this process, 142 to be precise. Rereading everything, I realized that many of my posts could use some improvements, spell-checking, better syntax, and improved graphics; needless to say, this got pretty overwhelming.

Although I aim to revamp all the posts, I deiced to get the site in a minimal yet presentable state before going any further. There are some key aspects I still need to decide on that require some non-trivial work (e.g. hosting pictures, comments, search, categories) .

I'll be gradually updating more of the old posts and publishing them in weeks to come. I do have also some new posts in mind regarding my latest projects and things I learned in the past five years.
