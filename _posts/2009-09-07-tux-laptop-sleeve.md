---
layout: post
status: darft
published: true
title: Tux Laptop Sleeve
author: Carlos
id: 82
date: '2009-09-07 11:50:50 -0400'
date_gmt: '2009-09-07 15:50:50 -0400'
categories:
- My Projects
tags:
- Linux
- Craft
---
The only defect of my new [Vostro 1320](http://carlitoscontraptions.com/2009/09/new-laptop/ "Vostro 1320") is that it did not come with a sleeve as the Eee PC does. Since I would like to protect the laptop from scratched and dust, a sleeve is absolutely required. So, from my perspective, the only solution to this problem was to do one myself.

I had an old blazer from my girlfriend's father that was too oddly shaped to fit anybody I know, but I liked the fabric and I thought that it could become a very nice laptop sleeve. The only problem about that idea is that I do not know how to sew. Of course, not knowing how to do something, has never stopped me before and is not likely to stop me any time soon.

{% include fig.html img="sleeve_collage.jpg" caption="Tux laptop Sleeve" id=page.id %}

## Materials

*   Old blazer
*   Zipper
*   Fabric Marker (not required but rather cool)

## Doing it

Making this was surprisingly simple since I expected great difficulties coming from the sewing part. I decided to go with a design as simple as possible and to minimize the number of stitches, this resulted in making something extraordinary similar to a simple cousin.

I simply cut a rectangle big enough so that, when folded in half, it could contain my laptop from the blazer and then stitched the bottom and the side of the resulting pouch. Then, I added a zipper to the top. I used zigzag stitches all the time and, of course, I sewed the sleeve from the inside.

Also, I chose a section of the blazed which had an internal and external pocked so I could use them to carry some extra stuff. Furthermore, I kept the (synthetic?) silk interior lining that looks better than the bare exterior wool.

Finally, I wanted to ad some decorations to the pouch so I decided to use my brother's fabric markers (he is using for making pretty cool disguises) to ad the word "Linux" to one side of the sleeve. Then I asked him (since he is much more talented than me at drawing (and since I am very lazy) to draw Tux on the back. To achieve that he used a stencil made from the image below that I got from [the internet](http://noisymime.org/blog/?p=27 "Tux Stencil").

{% include fig.html img="carlitoscontraptions.com" caption="Tux Stencil" id=page.id %}

I think the final result is pretty cool and very useful. The pockets are great, I can put all of the things that I usually need to carry with my laptop (e.g. mouse, headphones, USB keys) without any troubles. The only thing missing is perhaps a handle that I may add in the future.

{% include fig.html img="brother.jpg" caption="My Brother Painting Tux" id=page.id %}