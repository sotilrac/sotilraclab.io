---
layout: post
status: darft
published: true
title: Simple Precision-Screwdriver Holder
author: Carlos
id: 92
wordpress_url: http://carlitoscontraptions.com/?p=434
date: '2009-12-14 15:45:32 -0500'
date_gmt: '2009-12-14 19:45:32 -0500'
categories:
- Tools
- My Projects
tags:
- Craft
- Wood
---
{% include fig.html img="img_1032.jpg" caption="Workshop wall" id=page.id %}

This is a very simple and quick hack that anybody can do. The objective is to build a holder that will keep all the precision screwdrivers in one place and facilitate the access to them (i.e. it should be easy to see which one is which and they should be easy to take).

## Materials

*   A plastic container cap (I love caps)
*   That's it, you do not need anything else

## Tools

*   Drill or strong scissors
*   Precision screwdrivers
*   A nail (maybe)

## Doing it

{% include fig.html img="IMG_2611.JPG" caption="Screwdriver Holder" id=page.id %}

1.  Drill or cut a big hole on the top of the cap. This hole will be used to suspend the screwdriver holder.
2.  Punch in the Screwdrivers in  the rim of the cap. If you do not trust your screwdrivers to pierce your cap, you may use a nail. Keep in mind that the screwdrivers have to be very snug in the holes since they are only being held-in by friction.
3.  Celebrate! you are done.