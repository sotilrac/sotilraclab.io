---
layout: post
status: darft
published: true
title: 'The Wind Turbine Part 2: Design and Materials'
author: Carlos
id: 573
wordpress_url: http://carlitoscontraptions.com/?p=573
date: '2010-05-10 01:52:46 -0400'
date_gmt: '2010-05-10 05:52:46 -0400'
categories:
- My Projects
tags:
- International Cooperation
---
Now that the [general idea](http://carlitoscontraptions.com/2010/02/the-wind-turbine-part-1-general-idea/ "Wind Turbine Part 1: General Idea") has been introduced, let us have a look at the materials and parts that make up the IBee Turbine design.

## The Materials

*   ~ 2 ft. of 8" Schedule 80 PVC pipe - For the blades
*   ~ 2 ft. of 6" 1/4 " thick PVC pipe - For the body
*   A desktop computer side panel - For the tail
*   An old AC motor - For the generator
*   A 1.5" x 3.5" x 8" block of wood - For holding the body to the mast
*   Several 6" and 8" stainless pipe clamps - For holding the motor and the block of wood to the body
*   A beautiful custom machined flywheel - For holding the blades
*   A 1" pipe flange - For holding the body to the mast
*   A 6" x 1" galvanized steel nipple - For the pivoting assembly
*   A 24" x 1" galvanized steel pipe - For the pivoting assembly
*   A 1" galvanized steel union - For the pivoting assembly
*   A 1" to 1.25" galvanized steel reduction - For the pivoting assembly
*   A 5 m 1.25" galvanized steel pipe - For the mast
*   Five steel brackets and appropriate wall screws - For holding the mast to the wall
*   A car battery - For the energy storage
*   A high power resistor - For the charge controller
*   A DPST switch - For the charge controller
*   A fuse - For the charge controller
*   A battery charge controller (for solar panels in our case) - For the battery charge control
*   A 75W (in our case) power inverter - For producing 110 VAC
*   A nice bedside table - For creating a nice "Energy" station.
*   ~ 10 m of two-conductor electrical cable - For connecting the Generator to the battery
*   An old computer power supply case - For holding all the electronics components together
*   Stainless steel screws, washers and lock washers, and a nail
*   A nylon cutting board (or at least a piece of it)
*   Cable ties
*   Lots of bees - No seriously, lots of them

{% include fig.html img="IMG_2836.jpg" caption="Main IBee Turbine Components" id=page.id %}

{% include fig.html img="IMG_2838.jpg" caption="The main components from another angle" id=page.id %}

## Making the parts

We started building the turbine before the trip. This way, we were able to make all the complex components (especially the blades) in a known environment with all the required tools and comfort. This was a rather good move since this spared a lot of work in Ecuador and allowed us to use tools that were not available down there (mainly power tools such as jig saws, belt sander, and of course the Dremel).

### Initial Gathering

In order to start the construction, we gathered at [PiÃ¨ces d'Auto Jarry](http://maps.google.ca/maps?layer=c&cbll=45.580455,-73.788764&cbp=12,97.27,,0,2.17&ved=0CBkQ2wU&ei=ZLx8S4KWMo6GzASBs-XkCA&ie=UTF8&hq=&hnear=5478+Avenue+Bourret,+Montr%C3%A9al,+Communaut%C3%A9-Urbaine-de-Montr%C3%A9al,+Qu%C3%A9bec&ll=45.580977,-73.788922&spn=0,359.97262&z=16&panoid=gwm0VGZgTiDg36BtysL9zw "PiÃ¨ces d'Auto Jarry"). Mr. Plante (the owner) was kind enough to allow us to use his tools and warehouse to build the blades and the turbine's body. On that construction day, the students worked very hard:

*   They made the blades following the [MAKE instructions](http://cachefly.oreilly.com/make/television/wind.pdf "MAKE Wind Turbine") using the 8" PVC pipe.
*   After realizing a smaller 6" pipe could fit almost perfectly the generator motor, they made the turbine body out of it. The pipe was brought in case we could not find the appropriate 8" pipe for the blades.
*   They made and tested the electrical connections for the generating motor. This meant spinning the motor shaft very fast by hand and hoping for an LED to turn ON.
*   What did I do meanwhile? Mainly look cool and be idle since they were working very well.

{% include fig.html img="Construction.jpg" caption="Construction Day" id=page.id %}

### The Finishing Touches

After the initial building session was done, my work started. I had to finish all the started parts as well as creating some new ones that were missing. It is important to note that all PVC pieces were coated with exterior paint. This not only made them look very pretty but also protected them against UV radiation which is very damaging for such materials.

### The Body

The turbine body is made form a 6" PVC pipe. The idea behind this for the body to be in one single piece that will hold the motor (generator) and the tale while being weatherproof.

The tube is slightly larger than the motor it carries and thus required three cuts on one end. These cuts and the natural flexibility of the tube allowed the motor to be held in place firmly when tying it with stainless steel pipe clamps.

{% include fig.html img="IMG_2790.jpg" caption="Closeup of the motor clamped to the body" id=page.id %}

Also, two cuts were made on the other end. This created a slit where the tale can slide in place. The tale was then held by using cable ties. Another slit was made in order to accommodate the various wires coming out form the motor.

Furthermore, two triangular cuts were made on each side of the body. This resulted in a (hopefully) more aerodynamic shape, and in a significant weight reduction. his is important since the torque exerted on the mast by a (roughly) 20 kg turbine can be quite considerable depending on its length.

It is important to note that all cuts in the body end in a circle. This is to prevent the cut from producing a crack because of the resulting weakening of the structure. I am not sure on how necessary (or effective) this is, but at least it gives me peace on mind.

{% include fig.html img="IMG_2833.jpg" caption="The Finished Turbine Body" id=page.id %}

### The Blades

The blades were cut by the students from an 8" schedule 80 PVC pipe by following the instructions in the [Make Video](http://blog.makezine.com/archive/2009/02/make_television_episode_7_urban_pro.html "Make Wind Generator"). I simply added the finishing touches that included: smoothing them further, making sure they all weighted the same and had the same dimensions, drilling the mounting holes so they could be attached to the flywheel, and painting them.

> **Quick Tip:** The back of a box cutter is ideal for smoothing PVC (and other plastics I assume). Simply run it trough the surface back and forth until you get the desired result. I found this to be more effective than any other tool.

{% include fig.html img="Construction2.jpg" caption="Making the blades" id=page.id %}

Also, the mounting holes were drilled taking into account the fact that the blade tips need to be equidistant from one another once mounted on the wheel.

{% include fig.html img="IMG_2823.jpg" caption="The Blades" id=page.id %}

### The Beautiful Flywheel

The [CIMME](http://www.cimme.ca/ "CIMME") machined a beautiful flywheel based on a [Sketchup drawing](http://carlitoscontraptions.com/wp-content/uploads/2010/02/flywheel.skp "flywheel.skp") I provided. To my great surprise the flywheel turned out to be perfect and to fit flawlessly in the design. We had this piece professionally machined since it is crucial for the blades to be held firmly to the motor spindle. Also, its dimensions have to be very precise in order to achieve optimal balance and prevent any vibration or wobbling of the blades.

{% include fig.html img="flywheel_iso.png" caption="Flywheel Isometric View" id=page.id %}

The flywheel was machined in aluminum using a CNC mill and sandblasted afterwards in order to increase its coolness factor.

{% include fig.html img="Construction12.jpg" caption="The Flywheel" id=page.id %}

### The Pivot

One of the biggest challenges to me was to imagine a good way of coupling the turbine to the mast that holds it up. The idea came to me that using a block of wood, some pipe clamps and a pipe flange would be a rather simple and sturdy solution.

First, it was necessary to carve out a cylinder from the block of wood in order to be able to properly fasten if to the turbines body. This was done manually using mainly a wood knife.

{% include fig.html img="IMG_2519.jpg" caption="First wood attachment prototype" id=page.id %}

The preliminary version of the wood coupling evolved into a more sophisticated one as shown below. The latter is larger (increasing the assembly robustness), and features a hole and some cuts in order to allow the motor cables to go trough.

{% include fig.html img="IMG_2829.jpg" caption="The wooden attachment" id=page.id %}

The actual pivoting part was done by using a 1" galvanized steel pipe union. It was modified in such a way that it could turn (almost) freely. This was achieved by not screwing the union completely and adding a hole and a nail that acts similarly to a clevis pin and prevents the union to get unscrewed. Also, a nylon washer was added in between the the rotating metal parts in order to reduce friction and prevent premature wear. The washer was cut form an old cutting board. Finally, in order to attach this rotating part to the wooden attachment, a pipe flange and a nipple were used.

{% include fig.html img="IMG_2825.jpg" caption="The pivoting part" id=page.id %}

### The Tail

The wind turbine requires a tail so it can follow the direction of the wind. For this purpose we used an old computer side panel that we cut, drilled, and painted. The tail fits into a slit cut in the body as mentioned before and was fastened with four cable ties.

{% include fig.html img="IMG_2831.jpg" caption="The finished tail" id=page.id %}

**Coming up:** [_Part 3: Building the Generator_](http://carlitoscontraptions.com/2010/05/the-wind-turbine-part-3-building-the-generator/)