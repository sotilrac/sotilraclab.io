w---
layout: post
status: darft
published: true
title: Arduino Clones
author: Carlos
id: 33
date: '2007-08-03 09:48:00 -0400'
date_gmt: '2007-08-03 16:48:00 -0400'
categories:
- General
- Arduino
tags: []
---
{% include fig.html img="Arduino+package.jpg" caption="" id=page.id %}

For my birthday my girlfriend got me 4 Bare-Bone Boards (from the [Modern Devices Company](http://www.moderndevice.com/)). The boards are fully featured [Arduino](http://www.arduino.cc/) clones. The only difference with the original Arduino (AFAIK) is that they are cheaper (15$ each or less) and better suited for breadboard connection.

## What's an Arduino?  

> Arduino is an open-source electronics prototyping platform based on flexible, easy-to-use hardware and software. It's intended for artists, designers, hobbyists, and anyone interested in creating interactive objects or environments.
> 
> \- [The Arduino Website](http://www.arduino.cc/) -

{% include fig.html img="Arduino+parts.jpg" caption="" id=page.id %}

Note that these clones are cheaper because they require a USB-to-TTL serial cable (20 $). The advantage of this approach is that the cable includes the required USB controller chip and can be used to program many boards. In short, you only need to pay for the USB connectivity once and get to use it on as may boards as you want.

Also, I was very (really very) pleased to see that the Arduino software works perfectly under Linux and that there are [instructions for installation on all major distros](http://www.arduino.cc/playground/Learning/Linux) (including [Ubuntu](http://www.ubuntu.com/)) in the [Arduino website](http://www.arduino.cc/).

{% include fig.html img="Assembled+Arduino.jpg" caption="" id=page.id %}

Update: Here are a few extra shots done with my new camera. This is the new Rev. D board.

{% include fig.html img="ss851468.jpg" caption="" id=page.id %}

{% include fig.html img="ss851467.jpg" caption="" id=page.id %}