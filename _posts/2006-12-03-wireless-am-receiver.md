---
layout: post
status: darft
published: true
title: Wireless AM receiver
author: Carlos
id: 15
date: '2006-12-03 00:37:00 -0500'
date_gmt: '2006-12-03 07:37:00 -0500'
categories:
- Work in progress
tags: []
---

This is the schematics for a wireless AM receiver composed of a tuned amplifier, a AM demodulator, and an audio amplifier.

{% include fig.html img="wireless AM receiver.jpg" caption="" id=page.id %}  

{% include fig.html img="IMG_0028.jpg" caption="" id=page.id %}

This is the radio receiver implemented on a breadboard.

I'll post more details soon.