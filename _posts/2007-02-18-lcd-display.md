---
layout: post
status: darft
title: LCD Screen
author: Carlos
id: 21
date: '2007-02-18 11:25:00 -0500'
date_gmt: '2007-02-18 18:25:00 -0500'
categories:
- General
tags: []
---
I got a brand new LCD screen for free from the guys at [uC Hobby](http://www.uchobby.com/) (their website has many nice projects).

{% include fig.html img="LCD3.jpg" caption="" id=page.id %}

It is a Samsung 0202A ([see more details here](http://www.uchobby.com/index.php/2006/12/12/free-graphic-lcd/)). It is a very nice piece of equipment and I hoe I'll be able to put it to good use

I'll learn how to use it and I'll come up with some project for it soon enough (hopefully). Meanwhile, I'm open to any project ideas.