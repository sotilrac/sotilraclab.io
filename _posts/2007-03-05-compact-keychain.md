---
layout: post
status: draft
title: Compact Keychain
author: Carlos
id: 23
date: '2007-03-05 21:40:00 -0500'
date_gmt: '2007-03-06 04:40:00 -0500'
categories:
- Projects
tags: DIY
---
**Update:** I have done an improved version: [the compact keychain 2.0](http://carlitoscontraptions.com/2009/03/compact-keychain-20/ "Compact Keychain 2.0").

This is a nice keychain I've been using for more than a year now. Since it broke, I had to rebuild it so I took this chance to share it with the rest or the world.

## Materials

*   3 washers (number of keys + 1)
*   Some keys (I use only two keys)
*   A pop rivet (requires a river gun)

{% include fig.html img="keys.JPG" caption="" id=page.id %}

## Construction

1.  Fasten the keys together using the rivet and put washers between them.
2.  Admire your creation.

{% include fig.html img="keys2.JPG" caption="" id=page.id %}

That's it. It works well with my two keys, and it is very compact and quiet.

{% include fig.html img="keys3.JPG" caption="" id=page.id %}