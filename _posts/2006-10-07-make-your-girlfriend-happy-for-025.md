---
layout: post
status: darft
title: Make Your Girlfriend Happy For 0.25$
author: Carlos
id: 8
date: '2006-10-07 22:33:00 -0400'
date_gmt: '2006-10-08 05:33:00 -0400'
categories:
- My Projects
tags: []
---
I saw in [this site](http://cleanstream.net/mirrors/coin_ring/) (try [this one](http://homepage.mac.com/johnhuber/CoinRing/PhotoAlbum20.html) if the link is broken) that we can build a coin ring using a spoon, a drill, a Dremel and some metal polish. Immediately, I wanted to do one.

## Materials:  

*   An ordinary 0.25$ coin (Canadian in my case)
*   Lots of patience

{% include fig.html img="img_1255.jpg" caption="Canadian Quarter" id=page.id %}

## Making it happen  

*Warning*: your family and specially your dog could become agitated if you hit a coin with a spoon during an entire day.

I started by hitting my coin on its edge with a spoon. I used a small metal plate a found laying on the street as a rigid surface for the hitting. After about 2 hour of lots of hitting and almost no change I tried using a small hammer. It bended the coin faster but the results were a little deceiving. In the original site pictures, you see the inside of the coin bending uniformly outward., Instead I got flat edge and a smashed inside. After about two more hours of hitting I switch to a normal full-sized hammer.

{% include fig.html img="hammer+coin.jpg" caption="" id=page.id %}

I hit the coin until I got it to the right diameter (my estimation of my girlfriend's finger diameter) and then I hit it with the spoon once again. The spoon flattens the edge surface making it smoother (which means less sanding and polishing afterwards). Also the spoon allows for fine control over the shape of the ring.

{% include fig.html img="coin.jpg" caption="" id=page.id %}

Once I got the edge to the right shape and diameter, I drilled a hole on the coin. I then used a screw to secure the coin to a drill and polished the outside of the rim by making it spin very fast. I used a fine sandpaper at first and some Brasso and a cloth afterwards.

{% include fig.html img="coin+spining.jpg" caption="" id=page.id %}

{% include fig.html img="screwed+coin.jpg" caption="" id=page.id %}

Removing the inside of the coin was the most difficult part since I don't have any clamp that can hold it without damaging it. Also, I could not preserve the outer part of the coin's inside as shown in the original site since mine was completely smashed. Anyways, in the end I removed the body of the coin usind some files and polished the inside of the newly born ring.

{% include fig.html img="ring.jpg" caption="" id=page.id %}

I gave the shiny (except for some dark spots caused by my inaccurate hitting) ring to my girlfriend who was very happy with it. I was happy to see it fitted her finger (although it was a bit loose).

Later, I discovered that the guy in the site used a silver coin (I know it is an obvious fact), which I suppose is easier to bend than my 94% steel coin.

Anyway, the end result is quite pleasing and nobody will believe you built a ring from a coin by hitting it with a spoon (unless you explain them how to proceed and show them pictures, people are very skeptical).

{% include fig.html img="ring2.jpg" caption="" id=page.id %}

And yes I know, it is illegal to break money. This is why this never happened , you simply imagined visiting this site and seeing this project.