---
layout: post
status: darft
published: true
title: 'I''ve Got An X-Ray Machine!'
author: Carlos
id: 968
wordpress_url: http://carlitoscontraptions.com/?p=968
date: '2011-06-15 21:44:47 -0400'
date_gmt: '2011-06-16 01:44:47 -0400'
categories:
- General
tags: []
---
I just found an X-Ray machine in the garbage. Yes, you read right, I now have one of those X-Ray machines like the ones your dentist uses.

{% include fig.html img="IMG_0224.jpg" caption="The X-Ray Machine is watching You" id=page.id %}

Of course, I have no experience whatsoever with X-Ray machines so I am open to suggestions. What should I do with it? Any suggestions? Any projects that you would like to see me do with it?

Also, how safe is operating this thing? I guess I will need a lead jacket.

Here are more details about the machine, it is a General Electric, and comes with an arm that makes it easy to handle since it is extremely heavy. The arm is lovely, the only problem is that it is heavier than the machine and needs to be wall mount. Needless to say, I have no walls that could endure so much torque.

{% include fig.html img="IMG_0222.jpg" caption="X-Ray machine With Arm and Weird Base" id=page.id %}

The arm is designed to transfer pretty much all of its weight and that of the X-Ray generator into the wall. This makes it very convenient and also impossible to install in a regular house. At least, as far as I can imagine for now.

I also found what seems to be a very heavy base but I am not sure what it is suppose to do. nevertheless, I always have a good use for a large square steel plate.

{% include fig.html img="IMG_0223.jpg" caption="X-Ray Machine from the garbage" id=page.id %}

Let me know your ideas and suggestions by posting a comment below.