---
layout: post
status: darft
published: true
title: 1000 Visits!
author: Carlos
id: 13
date: '2006-11-15 18:15:00 -0500'
date_gmt: '2006-11-16 01:15:00 -0500'
categories:
- General
tags: []
---
{% include fig.html img="gmail.gif" caption="" id=page.id %}

I know 1000 is not that many visits but it is still a good opportunity to celebrate. This is why I'm hosting a Gmail invites giveaway. Basically, if you want a Gmail invite just post a comment saying so and I'll give it to you. Be sure to include your email.

By the way, if you don't want to get spammed, use [trashmail](http://trashmail.net/) or write your email so spammers cannot [gather it](http://en.wikipedia.org/wiki/E-mail_spam#Gathering_of_addresses) (i.e. if your email is example@somehost.com, you can write it as "example AT somehost DOT com").

I know Gmail invites are getting old but I have some invites left and I would like to share them.