---
layout: post
status: darft
title: Sugar Trick
author: Carlos
id: 29
date: '2007-05-20 20:34:00 -0400'
date_gmt: '2007-05-21 03:34:00 -0400'
categories:
- My Projects
tags: []
---
I read that one can erase the branding on products by using a sugar cube and a bit of saliva (I'm guessing the saliva part). This came handy when I got a free 128MB USB key in a DVD-R pack.

This is the key in it's original state.

{% include fig.html img="stick+before.jpg" caption="" id=page.id %}

By rubbing a cube of sugar (lubricated with some handy saliva), I easily removed the branding without scratching the plastic.

{% include fig.html img="stick+sugar.jpg" caption="" id=page.id %}

After a bit of cleaning I go a nice USB key without any branding and no traces of saliva.

{% include fig.html img="stick+after.jpg" caption="" id=page.id %}
