---
layout: post
status: darft
published: true
title: Kubuntu Karmic Koala is out!
author: Carlos
id: 87
date: '2009-10-29 15:20:52 -0400'
categories:
- Software
tags:
- Software
- Open Source
- Linux
---
{% include fig.html img="kubuntu_Karmic.jpg" caption="Kubuntu Karmic Koala" id=page.id %}

[Kubuntu Karmic Koala](http://www.kubuntu.org/news/9.10-release "Kubuntu 9.10") is finally out! I use it since the [Release Candidate](http://www.kubuntu.org/news/9.10-rc "Kubuntu 9.10 RC") came Oct. 22 nd, and it is absolutely awesomely mind-blowingly fabulous. All of the kinks in Jaunty have been fixed and a lot of new features have been added.

## Kubuntu?

Why am I talking about [Kubuntu](http://www.kubuntu.org/ "Kubuntu") and not about its more popular sibling [Ubuntu](http://www.ubuntu.com/ "Ubutu")? Well, very simply because [KDE](http://kde.org/ "KDE") kicks Gnome's ass any day (while blindfolded and with all of its finger stuck in its nose). I know that seems like a very bold and unjustified statement, well it is indeed very bold but totally justified.

The main difference about KDE and Gnome, besides the fact that the KDE foundation is much more solid, flexible and portable, is the mindset. In KDE you can configure (trough a nice GUI) pretty much everything, whereas in Gnome, you get a bunch of very comfortable defaults that (although they can be modified) are not intended to be fiddled with too much.

Also, KDE is much more than a desktop environment and provides a full suite of programs that do almost everything you could want to do. These programs also integrate very well together and provide as many more features and options than any sane person would need or be able to use (but who likes sane people anyway?).

## Quick Review

{% include fig.html img="kde_desktop_grid.png" caption="My Desktops (Grid View)" id=page.id %}

I am currently using the 64-bit version of Kubuntu and it is performing incredibly well. The system ([my laptop](http://carlitoscontraptions.com/2009/09/new-laptop/ "Dell Vostro 1320")) boots in around 40 seconds and turns off in less than 15 seconds. The graphical performance is flawless and I can benefit from smooth performance even when doing very processor intensive tasks (such as [stitching photos together](http://carlitoscontraptions.com/2009/10/automatic-panoramas-in-montreal/ "Making Panoramas")).

Also, It comes with [Ubutu One](https://one.ubuntu.com/ "Ubuntu One") (a remote storage service) which is pretty convenient for sharing and backing up files.

I'll try to do a screencast and post it in order to show off the Koala.