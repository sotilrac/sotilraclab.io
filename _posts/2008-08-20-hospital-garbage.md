---
layout: post
status: darft
published: true
title: Hospital Garbage
author: Carlos
id: 52
date: '2008-08-20 20:40:00 -0400'
date_gmt: '2008-08-21 03:40:00 -0400'
categories:
- My Projects
tags:
- Trash
- Craft
---
I found a little stool lying around in the garbage lately and decided to give it a new life. It turns out the stool comes from some kind of hospital and is actually pretty old.

{% include fig.html img="ss852261.jpg" caption="" id=page.id %}

In order to refurbish the stool, I simply applied a coat of anti rust spray paint, oiled the screw holding the seat, and put in some new rubber feet. My grandmother, who was visiting (from Argentina) was kind enough to make a new cushion for it.

This build was really simple but resulted in a nice, modern looking stool for almost no cost.
