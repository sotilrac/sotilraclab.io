---
layout: post
status: darft
published: true
title: DIY Birthday Gift (a.k.a. Nail Puzzle)
author: Carlos
id: 37
date: '2007-08-29 19:01:00 -0400'
date_gmt: '2007-08-30 02:01:00 -0400'
categories:
- My Projects
tags: []
---
## Hypothetical situation

Let's suppose tomorrow is your friend's birthday and you forgot to get him/her a present. Also, let's assume you have no money whatsoever, all nearby stores are closed and s/he doesn't like things that come from the garbage.

## What to do?  
a. You don't give her/him any present at all (second best option).  
b. You make a gift yourself (best option).  
c. You go into exile.

I chose "b". Below, instruction on how to make a quick and nice puzzle.

{% include fig.html img="nail+puzzle2.JPG" caption="" id=page.id %}

## Materials

*   19 big nails
*   a piece of wood

## Tools

*   hammer
*   sand paper
*   sanding machine (particularly useful, but optional)
*   glue
*   drill
*   jig saw

In order to build a nice puzzle that will...well puzzle your friend for a while, especially if s/he is an engineering student, do as follows:

1.  Cut a large piece of wood that will become the main board
2.  Cut a small block that will stand and the nail stand.
3.  Glue them together so they look nice and the nail stand is not in the way
4.  Drill holes of equal depth on the small wood block so 18 nails can stand separately in each hole
5.  Plant a nail in the middle of the board and make sure it is nice and vertical.
6.  If you have some printable labels you can print the game instruction and stick them on an empty spot on the wood board. Make sure you sand everything well before applying the labels.
7.  It should look like the picture below.

{% include fig.html img="nail+puzzle.JPG" caption="" id=page.id %}

Game Instructions: Put all 18 nails on the central nail. The resulting structure should be stable.  

This means they should all be placed so that they only touch the central nail's head and maybe each other.

Game Solution: You can find the puzzle solution [here](http://google.com/).

Now enjoy your new puzzle.[_](http://3.bp.blogspot.com/_940DBYqYeYo/RtYxrwGzkRI/AAAAAAAAAXg/QyfFmWJalSI/s320/sdxfbdfbdfh.JPG)